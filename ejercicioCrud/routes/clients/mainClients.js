var express = require('express');
var router = express.Router();
var path = require('path');
var fileData = require("fs");

var urlTxt = "../../files/dataHistory.txt"
var urlJson = "../../files/dataClients.json"
var jsonOk = require("../../files/ok.json");
var ok_delete = require("../../files/ok_delete.json");
var ok_modif = require("../../files/ok_modif.json");
/* GET users listing. */
router.use(function(req, res, next) {
    var host = req.get('origin');
    res.setHeader('Access-Control-Allow-Origin', host||"*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE');
    next();
});

router.get('/', function(req, res, next) {
  res.render('clients', { title: '' });
});

function WriteDataToFileJson(list) {
	var filePath= path.join(__dirname, urlJson);
	var tmp = fileData.readFileSync(filePath, 'utf8');
  	var data = list;
  	fileData.writeFile(filePath, data );
 }

 function WriteHistoryToFile(list, typeOperation) {
	var filePath= path.join(__dirname, urlTxt);
	var tmp = fileData.readFileSync(filePath, 'utf8');
  	var data = typeOperation + list + "\n";

  	fileData.truncate(filePath, 0, function() {
    	fileData.writeFile(filePath, tmp + data );
  	});
 }

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

function jsonToObj() {
	var list = {
  		'data' :[]
	};
	var filePath= path.join(__dirname, urlJson);
	var tmp = fileData.readFileSync(filePath, 'utf8');
	if(tmp.length > 0){
		var obj = JSON.parse(tmp);
		list = obj;
	}
	return list;
 }

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/v0/createClient', function(req, res, next) {
	var num = Math.floor(Math.random() * 10000);
	var list = {
  		'data' :[]
	};

	list = jsonToObj();
	list.data.push({
		"id": num,
		"Name": req.body.first_name,
	    "Last name": req.body.last_name,
	    "Email": req.body.email,
	    "Job title": req.body.job_title,
	    "Telephone": req.body.tel
 	});
	
  	var json = JSON.stringify(list); //Se obtiene la lista de objetos en Json
	WriteHistoryToFile(json, "Create :");
	WriteDataToFileJson(json);
	return res.json(jsonOk);
});

router.get('/v0/listClients', function(req, res, next) {
	var list = {
  		'data' :[]
	};
	list = jsonToObj();
	var jsonString = JSON.stringify(list);
	var jsonClients = JSON.parse(jsonString);
	return res.json(jsonClients);
});


router.post('/v0/modifyClient/:id', function(req, res, next) {
	var list = {
  		'data' :[]
	};
	var element; 
	list = jsonToObj();
	var dat = list.data;
	for(var i = 0; i < (dat.length); i++){
		if(dat[i].id == req.params.id){
			dat[i].Name = req.body.first_name;		
		}
	}
	list.data = dat;
	var json = JSON.stringify(list);
	WriteHistoryToFile(json, "Modify :");
	WriteDataToFileJson(json);
	return res.json(ok_modif);
});

router.post('/v0/deleteClient/:id', function(req, res, next) {
	var deleteId;
	var list = {
  		'data' :[]
	};
	var element; 
	list = jsonToObj();
	var dat = list.data;
	for(var i = 0; i < (dat.length); i++){
		if(dat[i].id == req.params.id){
			deleteId=i;
		}
	}
	list.data.splice(deleteId, 1);
	list.data = dat;
	var json = JSON.stringify(list);
	WriteHistoryToFile(json, "Delete :");
	WriteDataToFileJson(json);
	return res.json(ok_delete);
});

module.exports = router;
